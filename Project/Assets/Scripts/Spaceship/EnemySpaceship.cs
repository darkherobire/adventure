using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }

            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Sound.EnemyExploded);
            Debug.Assert(Hp <= 0,"Hp is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject,0.5f);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                SoundManager.Instance.Play(SoundManager.Sound.EnemyFire);
                fireCounter = 0;
            }
        }
    }
}