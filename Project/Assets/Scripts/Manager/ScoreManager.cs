using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;
        [SerializeField] private TextMeshProUGUI healthCountText;

        public static ScoreManager Instance { get; private set; }
        
        private GameManager gameManager;
        private int playerScore;
        public int health;

        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
            Count();
        }

        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
            playerScore = score;
        }

        private void Count()
        {
            GameManager.Instance.playerSpaceshipHp = health;
            healthCountText.text = $"Health {health}";
        }

        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot be bull");
            
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }

        private void OnRestarted()
        {
            finalScoreText.text = $"Player Score : {playerScore}";
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
            Count();
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
            healthCountText.gameObject.SetActive(!hide);
        }
    }

}