using System;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        
        //[SerializeField] private AudioSource audioSource;
        public static SoundManager Instance { get; private set; }
        
        public enum Sound
        {
            BGM,
            PlayerFire,
            EnemyFire,
            PlayerExploded,
            EnemyExploded
        }
        
        [Serializable]  
        public class SoundClip
        {
            public Sound sound;
            public AudioClip audioClip;
            [Range(0,1)]public float soundVolume; 
            public bool loop = false;
            [HideInInspector]
            public AudioSource audioSource;
        }

        public void Awake()
        {
            Debug.Assert(soundClips != null && soundClips.Length != 0, "sound clips need to be setup");
            //Debug.Assert(audioSource != null, "audioSource cannot be null");

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }

        //public void Play(AudioSource audioSource,Sound sound)
        public void Play(Sound sound)
        {
            var soundClip = GetSoundClip(sound);
            if (soundClip.audioSource == null)
            {
                Debug.Log("null");
                soundClip.audioSource = gameObject.AddComponent<AudioSource>();
            }
            soundClip.audioSource.clip = soundClip.audioClip;
            soundClip.audioSource.volume = soundClip.soundVolume;
            soundClip.audioSource.loop = soundClip.loop;
            soundClip.audioSource.Play();
        }

        private void Start()
        {
            Play(SoundManager.Sound.BGM);
        }

        private SoundClip GetSoundClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.sound == sound)
                {
                    return soundClip;
                }
            }
            return null;
            //return default(SoundClip);
        }
    }
}